﻿// C# Example
// Builds an asset bundle from the selected objects in the project view.
// Once compiled go to "Menu" -> "Assets" and select one of the choices
// to build the Asset Bundle
using UnityEngine;
using UnityEditor;
using System.Collections;

public class ExportAssetBundles : MonoBehaviour
{
	[MenuItem("Assets/Build AssetBundle From Selection - Track dependencies/Android")]
	static void ExportResourceAndroid ()
	{
		string bundlePath = Application.dataPath + "/../Bundles/all-atlas-android.unity3d";
		ExportResource(bundlePath, BuildTarget.Android);
	}

	[MenuItem("Assets/Build AssetBundle From Selection - Track dependencies/iPhone")]
	static void ExportResourceIphone ()
	{
		string bundlePath = Application.dataPath + "/../Bundles/all-atlas-iphone.unity3d";
		ExportResource(bundlePath, BuildTarget.iPhone);
	}
	
	[MenuItem("Assets/Build AssetBundle From Selection - Track dependencies/WebPlayer")]
	static void ExportResourceWebPlayer ()
	{
		string bundlePath = Application.dataPath + "/../Bundles/all-atlas.unity3d";
		ExportResource(bundlePath, BuildTarget.WebPlayer);
	}
	
	static void ExportResource (string path, UnityEditor.BuildTarget platform)
	{
		// Build the resource file from the active selection.
		Object[] selection = Selection.GetFiltered (typeof(Object), SelectionMode.DeepAssets);
		BuildPipeline.BuildAssetBundle (Selection.activeObject, selection, path, 
                              BuildAssetBundleOptions.CollectDependencies | BuildAssetBundleOptions.CompleteAssets, platform);
		Selection.objects = selection;
	}

	[MenuItem("Assets/Build AssetBundle From Selection - No dependency tracking")]
	static void ExportResourceNoTrack ()
	{
		// Bring up save panel
		string path = EditorUtility.SaveFilePanel ("Save Resource", "", "New Resource", "unity3d");
		if (path.Length != 0) {
			// Build the resource file from the active selection.
			BuildPipeline.BuildAssetBundle (Selection.activeObject, Selection.objects, path);
		}
	}
}
