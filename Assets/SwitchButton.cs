﻿using UnityEngine;
using System.Collections;

public class SwitchButton : MonoBehaviour {
	
	public UISprite bgSpirte;
	
	private bool isHD = false;
	private bool isPhoto = false;
	
	private string pictureName = "Paint";
	private string qualityName = "SD";
	
	private AssetBundle allAtlas;
	
	// Use this for initialization
	IEnumerator Start () {
		string bundleUrl = getBundleUrl();
		
		// Start a download of the given URL
		WWW www = WWW.LoadFromCacheOrDownload(bundleUrl, 1);
		
		// Wait for download to complete
		yield return www;
		
		allAtlas = www.assetBundle;
		Object[] objs = allAtlas.LoadAll(typeof(GameObject));
		foreach (Object item in objs) {
			GameObject gobj = item as GameObject;
			Debug.Log(gobj.name);
		}
				
		//----
		
		GameObject btna = GameObject.Find("UI Root (2D)/Camera/Anchor/Panel/ButtonA");
		UIEventListener.Get(btna).onClick = onSwitchPicture;
		
		GameObject btnb = GameObject.Find("UI Root (2D)/Camera/Anchor/Panel/ButtonB");
		UIEventListener.Get(btnb).onClick = onSwitchQuality;
		
		bgSpirte = this.GetComponent<UISprite>();
		
		changePictureTo(pictureName);
		changeQualityTo(qualityName);
		Debug.Log("Start Function: " + pictureName + qualityName);
	}
	
	
	public void onSwitchPicture(GameObject btn)
	{
		string labelText;
		
		if (isPhoto) {
			pictureName = "Paint";
			labelText = "Paint";
		}
		else {
			pictureName = "Photo";
			labelText = "Photo";
		}
		
		Debug.Log("Switch Picture: " + pictureName + qualityName);
		
		changePictureTo (labelText);
		
		btn.GetComponentInChildren<UILabel>().text = labelText;
		isPhoto = !isPhoto;
	}

	
	public void onSwitchQuality(GameObject btn)
	{
		string labelText;
		
		if (isHD) {
			qualityName = "SD";
			labelText = "SD now";
		}
		else {
			qualityName = "HD";
			labelText = "HD now";
		}
		
		Debug.Log("Switch Quality: " + pictureName + qualityName);
		
		changeQualityTo (labelText);
		
		btn.GetComponentInChildren<UILabel>().text = labelText;
		isHD = !isHD;
	}

	void changePictureTo (string labelText)
	{
		string atlasName = "Atlas" + pictureName + qualityName;
		//GameObject replacementAtlas = Resources.Load("_Atlas/" + atlasName) as GameObject;
		GameObject replacementAtlas = loadObjectFromBundle(atlasName);
		
		string refAtlasName = "Main Atlas " + pictureName;
		//GameObject referenceAtlas = Resources.Load("_Atlas/" + refAtlasName) as GameObject;
		GameObject referenceAtlas = loadObjectFromBundle(refAtlasName);
		
		bgSpirte.atlas = referenceAtlas.GetComponent<UIAtlas>();
		bgSpirte.atlas.replacement = replacementAtlas.GetComponent<UIAtlas>();
	}

	void changeQualityTo (string labelText)
	{
		string atlasName = "Atlas" + pictureName + qualityName;
		//GameObject replacementAtlas = Resources.Load("_Atlas/" + atlasName) as GameObject;
		GameObject replacementAtlas = loadObjectFromBundle(atlasName);
		
		UIAtlas referenceAtlas = bgSpirte.atlas;
		referenceAtlas.replacement = replacementAtlas.GetComponent<UIAtlas>();
	}
		
	public GameObject loadObjectFromBundle(string objName)
	{
		GameObject gobj = allAtlas.Load(objName, typeof(GameObject)) as GameObject;
		return gobj;
	}
	
	public string getBundleUrl()
	{
#if UNITY_EDITOR
		return "file://" + Application.dataPath + "/../Bundles/" + "all-atlas.unity3d";
#elif UNITY_ANDROID
		return "https://dl.dropboxusercontent.com/u/1324456/unity/all-atlas-android.unity3d";
#elif UNITY_IPHONE
		return "https://dl.dropboxusercontent.com/u/1324456/unity/all-atlas-iphone.unity3d";
#endif
	}
}
